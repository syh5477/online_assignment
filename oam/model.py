################################################################################
#
# [IIE6114-01 TERM PROJECT] Online Assignment Problem
# Model.py
#  Input model
#
################################################################################

import networkx
from networkx.algorithms import bipartite

class online_assignment_model :
    def __init__(self) :
        self.n = None
        self.on_list = None
        self.off_list = None
        self.w = None
        self.graph = None

        self.crnt_j = 0
        
    
    def extract_from_file(self, filename) :
        # FILE EXTRACTION
        with open(filename, 'r') as _file :
            # Vertex num
            self.n = int(_file.readline())

            # Edge weights
            self.w = list()
            for _ in range(self.n) :
                line = _file.readline().rstrip()
                tokens = line.split()
                self.w.append([float(token) for token in tokens])
            
        # GRAPH CONSTRUCTION
        self.graph = networkx.Graph()
        
        # Vertex labels
        self.on_list = ['r%d' % i for i in range(0, self.n)]
        self.off_list = ['l%d' % i for i in range(0, self.n)]
        self.graph.add_nodes_from(self.on_list)
        self.graph.add_nodes_from(self.off_list)

        # Edge weights (negated to get the optimal value)        
        for j, rj in enumerate(self.on_list) :
            for i, li in enumerate(self.off_list) :
                self.graph.add_edge(rj, li, weight= -self.w[j][i])


    def opt_val(self):
        opt_matching = bipartite.minimum_weight_full_matching(self.graph)
        ret_val = 0.0
        for j, rj in enumerate(self.on_list) :
            mat_left = opt_matching[rj]
            mat_idx = int(mat_left[1:])
            ret_val += self.w[j][mat_idx]
        return ret_val

    def reset(self) :
        self.crnt_j = 0

    def has_next(self) :
        return self.crnt_j < self.n

    def next(self) :
        if not self.has_next() :
            return None
        
        ret_list = []
        for i in range(0, self.n) :
            ret_list.append(self.w[self.crnt_j][i])
        self.crnt_j += 1
        return ret_list