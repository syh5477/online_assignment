import time

class online_assignment_solver:
    def __init__(self, model, algorithm, logfile) :
        self.model = model
        self.algorithm = algorithm
        self.rtlm = [-1 for _ in range(self.model.n)]          # right vertex to which each left is matched
        self.logfile = logfile
    
    def solve(self) :
        iter_num = 0
        while self.model.has_next() :
            wj = self.model.next()

            tick = time.perf_counter_ns()
            prscrptn = self.algorithm.run(wj)
            tock = time.perf_counter_ns()
            iter_num += 1
            self.logfile.write('%04d\t%d\n' % (iter_num, tock - tick))
            
            for k in range(len(prscrptn) // 2) :
                self.rtlm[prscrptn[2 * k + 1]] = prscrptn[2 * k]

            #for i in range(self.model.n) :
            #    if prscrptn[i] > -1 :
            #        self.rtlm[i] = prscrptn[i]

    def val(self) :
        ret_val = 0
        for i in range(self.model.n) :
            ret_val += self.model.w[self.rtlm[i]][i]
        
        return ret_val

        