import oam.algorithms.base as b

class greedy_algorithm(b.algorithm) :
    def __init__(self, n) :
        self.n = n
        self.is_left_exposed = [True for _ in range(n)]
        self.j = -1

    def run(self, wj) :
        self.j +=1
                
        max_i = -1
        max_w = -1.0        
        for i in range(self.n) :
            if(self.is_left_exposed[i]) :
                if max_w < wj[i] :
                    max_i = i
                    max_w = wj[i]
        
        #ret_list = [-1 for _ in range(self.n)]
        #ret_list[max_i] = self.j
        ret_list = [self.j, max_i]
        self.is_left_exposed[max_i] = False

        return ret_list