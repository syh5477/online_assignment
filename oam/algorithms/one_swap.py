import oam.algorithms.base as b

class basic_one_swap_algorithm(b.algorithm) :
    def __init__(self, n) :
        self.n = n
        self.rtlm = [-1 for _ in range(n)]
        self.w = []
        self.j = -1
    
    def run(self, wj) :
        self.j += 1
        self.w.append([wji for wji in wj])

        max_i = -1
        max_i_prime = -1
        max_w = -1.0
        for i in range(self.n) :
            # if i is exposed
            if self.rtlm[i] < 0 :
                if max_w < self.w[self.j][i] :
                    max_i = i
                    max_i_prime = -1
                    max_w = self.w[self.j][i]
                
                for i_prime in range(self.n) :
                    # if i_prime is matched
                    if self.rtlm[i_prime] > -1 :
                        aug_w = self.w[self.j][i_prime] \
                                - self.w[self.rtlm[i_prime]][i_prime] \
                                + self.w[self.rtlm[i_prime]][i]
                        if max_w < aug_w :
                            max_i = i
                            max_i_prime = i_prime
                            max_w = aug_w
        
        #ret_list = [-1 for _ in range(self.n)]
        if max_i_prime > -1 :
            max_j_prime = self.rtlm[max_i_prime]
            #ret_list[max_i] = max_j_prime
            #ret_list[max_i_prime] = self.j
            ret_list = [self.j, max_i_prime, max_j_prime, max_i]
            self.rtlm[max_i] = max_j_prime
            self.rtlm[max_i_prime] = self.j
        else :
            #ret_list[max_i] = self.j
            ret_list = [self.j, max_i]
            self.rtlm[max_i] = self.j

        return ret_list

class sorted_one_swap_algorithm(b.algorithm) :
    def __init__(self, n) :
        self.n = n
        self.rtlm = [-1 for _ in range(n)]
        self.w = []
        self.sorted_w = []
        self.wjptr = [0 for _ in range(n)]
        self.j = -1
    
    def run(self, wj) :
        self.j += 1
        self.w.append([wji for wji in wj])
        wj_w_idx = [(wji, i) for (i, wji) in enumerate(wj)]
        self.sorted_w.append(sorted(wj_w_idx, key=lambda x:x[0], reverse=True))

        max_i = -1
        max_i_prime = -1
        max_w = -1.0
        for i in range(self.n) :
            if self.rtlm[i] < 0 : # li is exposed
                if self.w[self.j][i] > max_w :
                    max_i = i
                    max_i_prime = -1
                    max_w = self.w[self.j][i]
            else : # li is matched
                j_prime = self.rtlm[i]
                while self.rtlm[self.sorted_w[j_prime][self.wjptr[j_prime]][1]] > -1 :
                    self.wjptr[j_prime] += 1
                new_wji, new_i = self.sorted_w[j_prime][self.wjptr[j_prime]]
                crnt_w = self.w[self.j][i] - self.w[j_prime][i] + new_wji
                if crnt_w > max_w :
                    max_i = new_i
                    max_i_prime = i
                    max_w = crnt_w
        
        #ret_list = [-1 for _ in range(self.n)]
        if max_i_prime > -1 :
            max_j_prime = self.rtlm[max_i_prime]
            #ret_list[max_i] = max_j_prime
            #ret_list[max_i_prime] = self.j
            ret_list = [self.j, max_i_prime, max_j_prime, max_i]
            self.rtlm[max_i] = max_j_prime
            self.rtlm[max_i_prime] = self.j
        else :
            #ret_list[max_i] = self.j
            ret_list = [self.j, max_i]
            self.rtlm[max_i] = self.j
        
        return ret_list