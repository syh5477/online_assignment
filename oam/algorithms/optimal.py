import networkx as nx
from networkx.algorithms import shortest_paths as nxsp

import oam.algorithms.base as b

class bellman_ford_optimal_algorithm(b.algorithm) :
    def __init__(self, n) :
        self.n = n
        #self.is_left_exposed = [True for _ in range(n)]
        self.j = -1

        self.res_graph = nx.DiGraph()
        self.res_graph.add_nodes_from(['l%d' % i for i in range(n)])
        self.res_graph.add_node('t')
        for i in range(n) :
            self.res_graph.add_edge('l%d' % i, 't', weight=0)
    
    def run(self, wj) :
        self.j += 1

        # insert new edges to the residual graph
        # negate the weight for reduction to the shortest path problem
        crnt_rj = 'r%d' % self.j
        self.res_graph.add_node(crnt_rj)
        for i in range(self.n) :
            self.res_graph.add_edge(crnt_rj, 'l%d' % i, weight = -wj[i])
        
        # print(self.res_graph.edges)

        # find the shortest path
        #pw_list, path_list = nxsp.single_source_bellman_ford(self.res_graph, crnt_rj)
        path_list = nxsp.single_source_bellman_ford_path(self.res_graph, crnt_rj)
        # print(pw_list)
        # print(path_list)
        #min_i = -1
        #min_pw = 1
        #min_path = None
        #for i in range(self.n) :
        #    if self.is_left_exposed[i] :
        #        li = 'l%d' % i
        #        if min_pw > pw_list[li] :
        #            min_i = i
        #            min_pw = pw_list[li]
        #            min_path = path_list[li]
        min_path = path_list['t']

        # augment the path & construct return list
        #ret_list = [-1 for _ in range(self.n)]
        ret_list = []
        #self.is_left_exposed[min_i] = False

        for k in range(len(min_path) - 1) :
            u = min_path[k]
            v = min_path[k+1]
            w = self.res_graph[u][v]['weight']
            self.res_graph.remove_edge(u, v)
            self.res_graph.add_edge(v, u, weight = -w)

            if k % 2 == 0 :
                j = int(u[1:])
                i = int(v[1:])
                #ret_list[i] = j
                ret_list.extend([j, i])
                
        
        # print(self.res_graph.edges)
        # print(ret_list)
        return ret_list
        

class dijkstra_optimal_algorithm(b.algorithm) :
    def __init__(self, n) :
        self.n = n
        self.a = [0 for _ in range(n)]
        self.b = [0 for _ in range(n)]

        self.res_graph = nx.DiGraph()
        self.res_graph.add_nodes_from(['l%d' % i for i in range(n)])
        self.res_graph.add_node('t')
        for i in range(n) :
            self.res_graph.add_edge('l%d' % i, 't', weight=0)
        
        self.j = -1
    
    def run(self, wj) :
        self.j += 1
        
        # insert new edges to the residual graph
        # negate the weight for reduction to the shortest path problem
        crnt_rj = 'r%d' % self.j
        self.res_graph.add_node(crnt_rj)
        
        # find proper bj
        self.b[self.j] = - min([-wj[i] - self.a[i] for i in range(self.n)])

        # insert edges to the residual graph with residual weights
        for i in range(self.n) :
            self.res_graph.add_edge(crnt_rj, 'l%d' % i, \
                weight = -wj[i] - self.a[i] + self.b[self.j])

        # find the shortest paths
        pw_list, path_list = nxsp.single_source_dijkstra(self.res_graph, crnt_rj)

        # update a's and b's
        for iprime in range(self.n) :
            liprime = 'l%d' % iprime
            self.a[iprime] += pw_list[liprime]
    
        for jprime in range(self.j + 1) :
            rjprime = 'r%d' % jprime
            self.b[jprime] += pw_list[rjprime]
        
        for jprime in range(self.j + 1) :
            for iprime in range(self.n) :
                rjprime = 'r%d' % jprime
                liprime = 'l%d' % iprime
                if liprime in self.res_graph[rjprime] :
                    self.res_graph[rjprime][liprime]['weight'] += pw_list[rjprime] - pw_list[liprime]
                # else :
                #    self.res_graph[liprime][rjprime]['weight'] += pw_list[liprime] - pw_list[rjprime]

        
        # augment the shortest path from j to t & construct return list
        #ret_list = [-1 for _ in range(self.n)]
        ret_list = []
        min_path = path_list['t']

        for k in range(len(min_path) - 1) :
            u = min_path[k]
            v = min_path[k+1]
            w = self.res_graph[u][v]['weight']
            self.res_graph.remove_edge(u, v)
            self.res_graph.add_edge(v, u, weight = -w)
            
            if k % 2 == 0 :
                j = int(u[1:])
                i = int(v[1:])
                #ret_list[i] = j
                ret_list.extend([j, i])
            
        return ret_list