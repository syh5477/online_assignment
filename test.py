import os
import sys
from oam.model import online_assignment_model
from oam.solver import online_assignment_solver
from oam.algorithms.greedy import greedy_algorithm
from oam.algorithms.one_swap import basic_one_swap_algorithm, sorted_one_swap_algorithm
from oam.algorithms.optimal import bellman_ford_optimal_algorithm, dijkstra_optimal_algorithm

instance_dirname = 'instance'
result_dirname = 'result'
log_dirname = 'log'

n = int(sys.argv[1])
exp = int(sys.argv[2])
p = int(sys.argv[3])
t = int(sys.argv[4])

in_name = 'inst_%04d_%02d_%03d_%02d.in' % (n, exp, p, t)
in_path = os.path.join(instance_dirname, in_name)

model = online_assignment_model()
model.extract_from_file(in_path)

# TRUE OPT
opt_val = model.opt_val()
out_name = 'result_%04d_%02d_%03d_%02d.out' % (n, exp, p, t)
out_file = open(os.path.join(result_dirname, out_name), 'w')
out_file.write('TRUE OPT \t%d\n' % opt_val)

# GREEDY  
model.reset()
log_name = 'log_%04d_%02d_%03d_%02d_greedy.out' % (n, exp, p, t)
log_file = open(os.path.join(log_dirname, log_name), 'w')
alg = greedy_algorithm(model.n)
solver = online_assignment_solver(model, alg, log_file)
solver.solve()
log_file.close()
out_file.write('GREEDY  \t%d\t%.02f\n' % (solver.val(), solver.val() / opt_val))

# OS BASIC
model.reset()
log_name = 'log_%04d_%02d_%03d_%02d_osbasic.out' % (n, exp, p, t)
log_file = open(os.path.join(log_dirname, log_name), 'w')
alg = basic_one_swap_algorithm(model.n)
solver = online_assignment_solver(model, alg, log_file)
solver.solve()
log_file.close()
out_file.write('OS BASIC\t%d\t%.02f\n' % (solver.val(), solver.val() / opt_val))

# OS BASIC
model.reset()
log_name = 'log_%04d_%02d_%03d_%02d_ossort.out' % (n, exp, p, t)
log_file = open(os.path.join(log_dirname, log_name), 'w')
alg = sorted_one_swap_algorithm(model.n)
solver = online_assignment_solver(model, alg, log_file)
solver.solve()
log_file.close()
out_file.write('OS SORT \t%d\t%.02f\n' % (solver.val(), solver.val() / opt_val))

# OPT BF  
model.reset()
log_name = 'log_%04d_%02d_%03d_%02d_optbf.out' % (n, exp, p, t)
log_file = open(os.path.join(log_dirname, log_name), 'w')
alg = bellman_ford_optimal_algorithm(model.n)
solver = online_assignment_solver(model, alg, log_file)
solver.solve()
log_file.close()
out_file.write('OPT BF  \t%d\t%.02f\n' % (solver.val(), solver.val() / opt_val))

# OPT DIJK
model.reset()
log_name = 'log_%04d_%02d_%03d_%02d_optdijk.out' % (n, exp, p, t)
log_file = open(os.path.join(log_dirname, log_name), 'w')
alg = dijkstra_optimal_algorithm(model.n)
solver = online_assignment_solver(model, alg, log_file)
solver.solve()
log_file.close()
out_file.write('OPT DIJK\t%d\t%.02f\n' % (solver.val(), solver.val() / opt_val))

out_file.close()