import os
import random

dirname = 'instance'

n = 1000
exp = 6
p = 1

max_w = 10 ** exp
max_i = 5

for i in range(max_i) :
    filename = 'inst_%04d_%02d_%03d_%02d.in' % (n, exp, int(p * 100), i + 1)
    with open(os.path.join(dirname, filename), 'w') as outfile :
        outfile.write('%d\n' % n)
        for l in range(n) :
            for r in range(n) :
                if random.random() <= p :
                    outfile.write('%d\t' % random.randrange(max_w))
                else :
                    outfile.write('%d\t' % 0)
            outfile.write('\n')