import subprocess

nlist = [100, 500, 1000]
explist = [3, 6]
plist = [.25, .5, .75, 1]
tlist = [(t + 1) for t in range(5)]

nlist = [500]
explist = [3]
plist = [.5, .75, 1]
tlist = [1]

for n in nlist :
    for exp in explist :
        for p in plist :
            for t in tlist :
                cmd = ['python', 'test.py', str(n), str(exp), str(int(p * 100)), str(t)]
                subprocess.run(cmd)